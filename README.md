# Specific Node Socket Identities 

## Custom send/receive roles and behaviours for webRTC protocols, specified by endpoints.  



![animation of holding two phones](socket_identity.gif)




### Installation 


Clone the repo. 

```bash
git clone https://gitlab.com/thornebrandt/socket-identities.git
```


```bash
cd socket-identities
npm install

```

Start the express server.

```bash
npm start
```


That's it!   The urls are the following:

* [https://localhost:8080/actor/](https://localhost:8080/actor/)
* [https://localhost:8080/director/](https://localhost:8080/director/)
* [https://localhost:8080/server/](https://localhost:8080/server/)
* [https://localhost:8080/audience/](https://localhost:8080/audience/)

These identities are arbitrary and can be changed for whatever scenario you'd like different endpoint roles for. 



![screenshot of a bunch of webcames](screenshot.png)



## Troubleshooting

Oops! - local https doesn't work. You will need to setup ssl somewhere.  


### Secure HTTPS (Remote Server) 


Oh no! You need https to open webcam in a browser!  
Which means you need an ssl certificate to share video streams over https. 

The following directions are if you already have a valid SSL certificate on your remote server.  
*( For local https development, and generation of an ssl certificate that is trusted by your entire computer, keep scrolling. )*

1. Get the `node-socket-server/server.js` running remotely on an https server with an ssl certificate and take note of the port the node process is running on. 

2. Change the public client [node-socket-server/public/index.js](https://gitlab.com/thornebrandt/webrtc_browser_unity/-/blob/secure_server/node-socket-server/public/index.js) to remote server where the node app is running.

3. If using a port other than 3000, change this in [node-socket-server/server.js](https://gitlab.com/thornebrandt/webrtc_browser_unity/-/blob/secure_server/node-socket-server/server.js)

4. In Unity, *Assets > Scenes > SampleScene*  in the  *SocketWebRTCController gameObject*, in the *Socket.IO Communicator Component*,  and change both the remote address to the remote address (just the name, leave out https), and make sure the box that says *Secure Connection*  "Using SSL / TLS? Do Not check this box if you are not using a publicly trusted SSL certificate for your server. is checked. 
^ Key word here is "publicly trusted."  This is why Unity isn't going to trust your self signed localhost SSL certificate if you've added it only to the browser.  
(If someone knows how to get Unity to force reading ssl certificates like a browser, let me know.)

5.  That should be it! Open up the page that is serving public/index.html and start unity. You now should be able to open it on phones! 

---

### Secure HTTPS for local development 

1. Check out the [secure_server](https://gitlab.com/thornebrandt/webrtc_browser_unity/-/commits/secure_server) branch. This will point the ports in the right direction for https. 

2. Install [openssl](https://www.openssl.org/)  either using PERL, but if you have linux or [wsl](https://docs.microsoft.com/en-us/windows/wsl/install)  it might already be installed.   Check by just typing `openssl`

3. Create a `certs` folder inside of `node-socket-server`   `node-socket-server/server.js` is pointing to files that will be generatted here. 

4. Generate root ssl certificate

```
openssl genrsa -des3 -out rootCA.key 2048
```
5. Enter passphrase and store it. 

6. Create root certificate with the generated key. 

```
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1460 -out rootCA.pem
```

7. Fill out all the information to your liking.

8. Trust the root ssl certificate. You need elevated access for this.  Open powershell as administrator, navigate to the `node-socket-server/certs` folder 

```
certutil -addstore -f "ROOT" rootCA.pem
```
You should get an added successfully command. 

9.  You can verify that this was added to root certificate authorities in Chrome by navigating to `Settings → Privacy and Security → Security → Manage Certificates → Trusted Root Certification Authorities.`

10. Create a `server.csr.cnf` file in the `certs` folder with the following. 

```
[req]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn

[dn]
C=US
ST=Illinois
L=Chicago
O=Yada Yada
OU=Yada Yada
emailAddress=yadayada@whatever.com
CN = localhost
```

11. Create a `v3.ext` file with the following: 

```
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = localhost
```


12. Use these files to create a signing request. 

```
openssl req -new -sha256 -nodes -out server.csr -newkey rsa:2048 -keyout server.key -config server.csr.cnf
```

13. A `server.key` file has been generated. 

14. Issue a certificate.   Remember the passphrase you wrote down earlier? Get that ready because it will ask for it. 

```
openssl x509 -req -in server.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out server.crt -days 500 -sha256 -extfile v3.ext
```
This will generate a `server.crt` file that translated your passphrase.  

`server.js` is already pointed towards these files with   

```
const options = {
    key: fs.readFileSync('certs/server.key'),
    cert: fs.readFileSync('certs/server.crt')
};
```

so you shouldn't have to change anything there.  


16. Assuming you haven't experienced any errors, this branch should be set up to work with `npm start` from the *node-socket-server* folder.    Open up "https://localhost:3000" and you should see a green lock sign which means that Chrome trusts this address. 

Use it the same as you did for the Installation instructions. 

For debugging, there is also a very simple server which you can start with `node test_ssl_server.js