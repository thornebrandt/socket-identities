// const socketServer = "https://localhost:8080";
const socketServer = "https://experiments.thornebrandt.com:8080";
let socket
let client_id;
const el = {};
let identity;
let room = {
    server: null,
    actor: null,
    director: null,
    audience: [],
};
let localMediaStream = null;
let videoEls;
let mediaConstraints = {
    actor: {
        video: {
            width: 320,
            height: 240,
            frameRate: 30,
        },
    },
    director: {
        video: {
            width: 320,
            height: 240,
            frameRate: 30
        }
    },
    server: {
        video: {
            width: 480,
            height: 640,
            frameRate: 30,
        },
    },
    audience: {
        video: {
            width: 320,
            height: 240,
            framerate: 30,
        }
    }
};



const { RTCPeerConnection, RTCSessionDescription } = window;

window.onload = async () => {
    el.numAudience = document.querySelector("#numAudience");
    el.actorContainer = document.querySelector("#actorContainer");
    el.directorContainer = document.querySelector("#directorContainer");
    el.serverContainer = document.querySelector("#serverContainer");
    el.textarea = document.querySelector("#message");
    el.localVideoContainer = document.querySelector("#localVideoContainer");
    //el.actorVideoContainer = document.querySelector("#actorVideoContainer");
    el.actorVideo = document.querySelector("#actorVideo");
    el.directorVideo = document.querySelector("#directorVideo");
    el.serverVideo = document.querySelector("#serverVideo");
    el.localVideo = document.querySelector("#localVideo");
    el.serverVideoContainer = document.querySelector("#serverVideoContainer");
    el.client_id = document.querySelector("#client_id");

    videoEls = {
        actor: el.actorVideo,
        director: el.directorVideo,
        server: el.serverVideo
    }

    identity = document.querySelector("body").getAttribute("id");
    await setupLocalVideo();
    initSocketConnection();
}

const updateRoomDisplay = () => {

    if (el.actorContainer != null) {
        if (room.actor != null) {
            el.actorContainer.innerHTML = room.actor.status + " : " + room.actor.message;
        } else {
            el.actorContainer.innerHTML = "waiting on actor...";
        }
    }

    if (el.directorContainer != null) {
        if (room.director != null) {
            el.directorContainer.innerHTML = room.director.status + ":" + room.director.message;
        } else {
            el.directorContainer.innerHTML = "waiting on director...";
        }
    }

    if (el.serverContainer != null) {
        if (room.server != null) {
            el.serverContainer.innerHTML = room.server.status + ":" + room.server.message;
        } else {
            el.serverContainer.innerHTML = "waiting on server...";
        }
    }

    el.numAudience.innerHTML = Object.values(room.audience).length;
}

const setupLocalVideo = async () => {
    if (el.localVideo != null) {
        localMediaStream = await getMedia(mediaConstraints[identity]);
        if (localMediaStream != null) {
            let videoStream = new MediaStream([localMediaStream.getVideoTracks()[0]]);
            el.localVideo.id = "localVideo";
            el.localVideo.autoplay = true;
            el.localVideo.width = mediaConstraints[identity].video.width;
            el.localVideo.height = mediaConstraints[identity].video.height;
            el.localVideo.playsInline = true;
            el.localVideo.srcObject = videoStream;
            el.localVideo.muted = true;
        } else {
            console.log("no webcam!");
        }
    }
}

const createRemoteVideo = (remoteIdentity, videoStream) => {
    videoEl = videoEls[remoteIdentity];
    if (videoEl != null) {
        videoEl.width = mediaConstraints[remoteIdentity].video.width;
        videoEl.height = mediaConstraints[remoteIdentity].video.height;
        videoEl.autoplay = true;
        videoEl.playsInline = true;
        videoEl.srcObject = videoStream;
        videoEl.muted = true;
    } else {
        console.log("could not find video element for " + remoteIdentity);
    }
};

async function getMedia(_mediaConstraints) {
    let stream = null;
    try {
        stream = await navigator.mediaDevices.getUserMedia(_mediaConstraints);
    } catch (err) {
        console.log("Failed to get user media!");
        console.warn(err);
    }
    return stream;
}



function addLocalTracksToPeerConnection(_stream, _pc, user) {
    if (_stream == null) {
        console.log("local User media stream not yet established!");
    } else {
        _stream.getTracks().forEach((track) => {
            _pc.addTrack(track, _stream);
        });
    }
}


const initSocketConnection = () => {
    console.log("attempting connection to " + socketServer);
    socket = io(socketServer, { secure: true });
    socket.on("connect", () => {
        console.log("socket.io connected");
    });

    socket.on("error", (e) => {
        console.log("error: " + e);
    });

    socket.on("introduction", (payload) => {
        iceServerList = payload.iceServers;
        //personal message from server after connection. 
        client_id = payload.id;
        console.log("this client id: " + client_id);
        socket.emit("set_identity", {
            identity,
            id: client_id,
            status: "connected",
            message: "browser version of " + identity,
        });
        el.client_id.innerHTML = identity + ":" + client_id;
        //loop through every user in room to make peer connection, and make call. 
    });

    socket.on("newUserConnected", (payload) => {
        let alreadyHasUser = false;
        if (payload.id === client_id) {
            connectToExistingClients(payload);
        } else {
            if (payload.identity === "audience") {
                room.audience[payload.id] = payload.room.audience[payload.id];
                room.audience[payload.id].peerConnection = createPeerConnection(room.audience[payload.id]);
                room.audience[payload.id].isAlreadyCalling = false;
            } else {
                room[payload.identity] = payload.room[payload.identity];
                room[payload.identity].peerConnection = createPeerConnection(room[payload.identity]);
                room[payload.identity].isAlreadyCalling = false;
            }
        }
        updateRoomDisplay();
    });

    socket.on("userDisconnected", (payload) => {
        if (payload.id !== client_id) {
            removeRemoteUserFromRoom(payload);
            updateRoomDisplay();
        }
    });

    socket.on("messageSent", (payload) => {
        if (payload.identity != identity) {
            if (room[payload.identity] != null) {
                room[payload.identity].message = payload.message;
                updateRoomDisplay();
            }
        }
    });

    socket.on("call-made", async (data) => {
        data.offer.type = convertUnityType(data.offer.type);
        const remoteUser = findUserInRoom(data.socket);
        if (remoteUser) {
            if (remoteUser.peerConnection) {

                await remoteUser.peerConnection.setRemoteDescription(
                    new RTCSessionDescription(data.offer)
                );

                const answer = await remoteUser.peerConnection.createAnswer();
                window.sdp = data.offer.sdp;

                await remoteUser.peerConnection.setLocalDescription(
                    new RTCSessionDescription(answer)
                );

                socket.emit("make-answer", {
                    answer,
                    to: data.socket,
                });
            } else {
                console.log("call made, but they don't have a peer connection", remoteUser);
            }

        } else {
            console.log("could not find user " + data.id + " in room", room);
        }
    });

    socket.on("answer-made", async (data) => {

        if (data.answer.type === "2" || data.answer.type === 2) {
            data.answer.type = "answer";
        }

        const remoteUser = findUserInRoom(data.socket);

        if (remoteUser != null) {
            await remoteUser.peerConnection.setRemoteDescription(
                new RTCSessionDescription(data.answer)
            );

            // preventing double calls. 
            if (!remoteUser.isAlreadyCalling) {
                callUser(data.socket);
                remoteUser.isAlreadyCalling = true;

            }
        }
    });

    socket.on("iceCandidateFound", (data) => {
        let user = findUserInRoom(data.socket);
        try {
            if (user.peerConnection != null) {
                user.peerConnection.addIceCandidate(data.candidate);
            } else {
                console.log("adding ice, but no peerConnection for", user);
            }
        } catch (err) {
            console.log("something went wrong after finding ice candidate: ", err);
        }
    });

    if (el.textarea != null) {
        el.textarea.addEventListener('input', function (event) {
            socket.emit("message", {
                id: this_client_id,
                message: event.target.value
            });
        }, false);
    }
}

function removeRemoteUserFromRoom(payload) {
    if (payload.identity == "audience") {
        Object.values(room.audience).forEach(audienceMember => {
            if (audienceMember.id === payload.id) {
                delete (room.audience[payload.id]);
            }
        });
    } else {
        if (payload.id === room[payload.identity].id) {
            room[payload.identity] = {
                status: "disconnected",
                message: "waiting on new " + payload.identity,
            }
        }
    }
}

function connectToRemoteIdentity(remoteIdentity) {
    if (room[remoteIdentity] != null) {
        remoteUser = room[remoteIdentity];
        remoteUser.peerConnection = createPeerConnection(remoteUser);
        remoteUser.isAlreadyCalling = false;
        callUser(remoteUser.id);
        remoteUser.isAlreadyCalling = true;
    }
}

function connectToAllAudienceMembers() {
    if (Object.values(room.audience).length) {
        Object.values(room.audience).forEach(audienceMember => {
            audienceMember.peerConnection = createPeerConnection(audienceMember);
            audienceMember.isAlreadyCalling = false;
            callUser(audienceMember.id);
            audienceMember.isAlreadyCalling = true;
        });
    }
}

function connectToExistingClients(payload) {
    room = payload.room;
    switch (identity) {
        case "audience":
            connectToRemoteIdentity("actor");
            connectToRemoteIdentity("director");
            connectToRemoteIdentity("server");
            break;
        case "actor":
            connectToAllAudienceMembers();
            connectToRemoteIdentity("server");
            connectToRemoteIdentity("director");
            break;
        case "director":
            connectToAllAudienceMembers();
            connectToRemoteIdentity("server");
            connectToRemoteIdentity("actor");
            break;
        case "server":
            connectToAllAudienceMembers();
            connectToRemoteIdentity("actor");
            connectToRemoteIdentity("director");
            break;
    }
}

function createPeerConnection(user) {
    let peerConnectionConfiguration;
    peerConnectionConfiguration = { iceServers: iceServerList, bundlePolicy: "max-compat" };
    let pc = new RTCPeerConnection(peerConnectionConfiguration);

    pc.ontrack = function ({ streams: [_remoteStream] }) {
        let videoStream = new MediaStream([_remoteStream.getVideoTracks()[0]]);



        console.log("ontrack", user);

        const remoteIdentity = user.identity;

        //watch any stream 
        createRemoteVideo(remoteIdentity, videoStream);
    }

    pc.onicecandidate = (e) => {

        if (e.candidate) {
            socket.emit("addIceCandidate", {
                candidate: e.candidate,
                to: user.id,
            });
        }
    }
    addLocalTracksToPeerConnection(localMediaStream, pc, user);
    return pc;
}


function findUserInRoom(user_id) {
    if (room.director != null && room.director.id === user_id) {
        return room.director;
    }
    if (room.actor != null && room.actor.id === user_id) {
        return room.actor;
    }
    if (room.server != null && room.server.id === user_id) {
        return room.server;
    }

    let foundAudienceMember = null;

    Object.values(room.audience).forEach(audienceMember => {
        //console.log(audienceMember);
        if (audienceMember.id === user_id) {
            foundAudienceMember = audienceMember;
            return;
        } else {
            console.log(audienceMember.id + " does not match: " + user_id);
        }
    });

    return foundAudienceMember;
}

async function callUser(_id) {
    let remoteUser = findUserInRoom(_id);
    if (remoteUser != null) {
        const offer = await remoteUser.peerConnection.createOffer();
        await remoteUser.peerConnection.setLocalDescription(
            new RTCSessionDescription(offer)
        );
        socket.emit("call-user", {
            offer,
            to: _id,
        });
    }
}


function convertUnityType(_type) {
    //dirty hack for unity enums. 
    if (_type === "0" || _type === 0) {
        return "offer";
    }

    if (_type === "2" || _type === 2) {
        return "answer";
    }
    return _type;
}



