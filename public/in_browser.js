// browser1


const lc = new RTCPeerConnection();
const dc = lc.createDataChannel("channel");
dc.onmessage = e => console.log("just got a message: ", e.data);
dc.onopen = e => console.log("connection opened!");
lc.onicecandidate = e => console.log("New Ice Candidate! Reprinting SDP", lc.localDescription);
lc.createOffer().then(o => lc.setLocalDescription(o)).then(a => console.log("created offer successfully."));
console.log(lc.singnalingState); //should be have-local-offer

//copy Ice candidate object. 



// browser2
// after getting string from offer. 
const offer = { replaceMe: "past ice candidate local description  here" };
const rc = new RTCPeerConnection();
rc.onicecandidate = e => console.log("New Ice Candidate! Reprinting SDP", rc.localDescription);
rc.ondatachannel = e => {
    rc.dc = e.channel;
    rc.dc.onmessage = e => console.log("New message from client! ", e.data);
    rc.dc.onopen = e => console.log("Connection OPENED!");
}
console.log("pre remote description: " + rc.signalingState); //stable

rc.setRemoteDescription(offer).then(a => console.log("offer set!"));

console.log("post remote description: " + rc.signalingState); //have-remote-offer

rc.createAnswer().then(a => rc.setLocalDescription(a)).then(a => console.log("answer created!"));

console.log("post create answer:" + rc.signalingState); //stable
//copy Ice candidate object. 


//browser 1 
const answer = { replaceMe: "paste browser 2 description here" };
lc.setRemoteDescription(answer);
// should now see "connection opened" in both browsers!
