const socketServer = "https://localhost:8080";
const debug = false; //TODO - add a querystring 
let socket
let this_client_id;
let el = {};
let clients = {};
let identity;
let room = {
    server: null,
    actor: null,
    director: null,
    audience: [],
};
let localMediaStream = null;
let videoEls;
let identityMediaConstraints = {
    actor: {
        video: {
            width: 320,
            height: 240,
            frameRate: 30,
        },
    },
    director: {
        video: {
            width: 320,
            height: 240,
            frameRate: 30
        }
    },
    server: {
        video: {
            width: 480,
            height: 640,
            frameRate: 30,
        },
    },
    audience: {
        video: {
            width: 320,
            height: 240,
            framerate: 30,
        }
    }
}

const { RTCPeerConnection, RTCSessionDescription } = window;
let iceServerList;

const videoWidth = 400;
const videoHeight = 300;
const videoFrameRate = 30;


let videoElement;

let consoleElement;

let mediaConstraints = {
    video: {
        width: videoWidth,
        height: videoHeight,
        frameRate: videoFrameRate,
    },
};


window.onload = async () => {
    el.numAudience = document.querySelector("#numAudience");
    el.actorContainer = document.querySelector("#actorContainer");
    el.directorContainer = document.querySelector("#directorContainer");
    el.serverContainer = document.querySelector("#serverContainer");
    el.textarea = document.querySelector("#message");
    el.localVideoContainer = document.querySelector("#localVideoContainer");
    //el.actorVideoContainer = document.querySelector("#actorVideoContainer");
    el.actorVideo = document.querySelector("#actorVideo");
    el.directorVideo = document.querySelector("#directorVideo");
    el.serverVideo = document.querySelector("#serverVideo");
    el.localVideo = document.querySelector("#localVideo");
    el.serverVideoContainer = document.querySelector("#serverVideoContainer");
    el.client_id = document.querySelector("#client_id");

    videoEls = {
        actor: el.actorVideo,
        director: el.directorVideo,
        server: el.serverVideo
    };

    identity = document.querySelector("body").getAttribute("id");

    //setupConsole();
    //localMediaStream = await getMedia(mediaConstraints);
    await setupLocalVideo();
    initSocketConnection();
};


const addToConsole = (_string) => {
    consoleElement.innerHTML += "<br>" + _string;
}


const setupConsole = () => {
    consoleElement = document.querySelector("#console");

    if (debug && "console" in window) {
        methods = [
            "log", "assert", "clear", "count",
            "debug", "dir", "dirxml", "error",
            "exception", "group", "groupCollapsed",
            "groupEnd", "info", "profile", "profileEnd",
            "table", "time", "timeEnd", "timeStamp",
            "trace", "warn"
        ];

        generateNewMethod = function (oldCallback, methodName) {
            return function () {
                var args;
                addToConsole(methodName + ":" + arguments[0]);
                args = Array.prototype.slice.call(arguments, 0);
                Function.prototype.apply.call(oldCallback, console, arguments);
            };
        };

        for (i = 0, j = methods.length; i < j; i++) {
            cur = methods[i];
            if (cur in console) {
                old = console[cur];
                console[cur] = generateNewMethod(old, cur);
            }
        }

    }
}

const updateRoomMessages = () => {

    if (el.actorContainer != null) {
        if (room.actor != null) {
            el.actorContainer.innerHTML = room.actor.message;
        } else {
            el.actorContainer.innerHTML = "waiting on actor...";
        }
    }

    if (el.directorContainer != null) {
        if (room.director != null) {
            el.directorContainer.innerHTML = room.director.message;
        } else {
            el.directorContainer.innerHTML = "waiting on director...";
        }
    }

    if (el.serverContainer != null) {
        if (room.server != null) {
            el.serverContainer.innerHTML = room.server.message;
        } else {
            el.serverContainer.innerHTML = "waiting on server...";
        }
    }

    el.numAudience.innerHTML = Object.values(room.audience).length;
}

function findUserInRoom(user_id) {
    if (room.director != null && room.director.id === user_id) {
        return room.director;
    }
    if (room.actor != null && room.actor.id === user_id) {
        return room.actor;
    }
    if (room.server != null && room.server.id === user_id) {
        return room.server;
    }
    return null;
}

const setupLocalVideo = async () => {
    if (el.localVideo != null) {
        //localMediaStream = await getMedia(mediaConstraints[identity]);
        localMediaStream = await getMedia(mediaConstraints);
        if (localMediaStream != null) {
            let videoStream = new MediaStream([localMediaStream.getVideoTracks()[0]]);
            el.localVideo.id = "localVideo";
            el.localVideo.autoplay = true;
            el.localVideo.width = mediaConstraints.video.width;
            el.localVideo.height = mediaConstraints.video.height;
            el.localVideo.playsInline = true;
            el.localVideo.srcObject = videoStream;
            el.localVideo.muted = true;
        } else {
            console.log("no webcam!");
        }
    }
}

const createRemoteVideo = (remoteIdentity, videoStream) => {
    if (remoteIdentity != "audience") {
        videoEl = videoEls[remoteIdentity];
        if (videoEl != null) {
            if (videoStream != null) {
                videoEl.width = mediaConstraints.video.width;
                videoEl.height = mediaConstraints.video.height;
                videoEl.autoplay = true;
                videoEl.playsInline = true;
                videoEl.srcObject = videoStream;
                videoEl.muted = true;
                videoEl.load();
                videoEl.play();
            } else {
                console.log("no video stream yet for " + remoteIdentity);
            }
        } else {
            console.log("could not find video element for " + remoteIdentity);
        }
    }
};

async function getMedia(_mediaConstraints) {
    let stream = null;
    try {
        stream = await navigator.mediaDevices.getUserMedia(_mediaConstraints);
    } catch (err) {
        console.log("Failed to get user media!");
        console.warn(err);
    }
    return stream;
}


function addTracksToPeerConnection(_stream, _pc) {
    if (_stream == null) {
        console.log("Local User media stream not yet established!");
    } else {
        _stream.getTracks().forEach((track) => {
            _pc.addTrack(track, _stream);
        });
    }
}

function moveClientToIdentity(_client, _identity, _message) {
    room[_identity] = _client;
    room[_identity].message = _message;
    if (_client.videoEl != null) {
        createRemoteVideo(_identity, _client.videoEl.srcObject);
    }
    _client.identity = _identity;
    updateRoomMessages();
}

function mergeExistingRoom(_room) {
    //_room is from the server.  room is global js client variable. 
    //meant for initialization and server's room dump only if values are null.

    if (room.actor == null && _room.actor != null) {
        room.actor = _room.actor;
        room.actor.message = room.actor.message || "actor connected";
        addIdentityToClients(room.actor);
    }
    if (room.director == null && _room.director != null) {
        room.director = _room.director;
        room.director.message = room.director.message || "director connected";
        addIdentityToClients(room.director);
    }
    if (room.server == null && _room.server != null) {
        room.server = _room.server;
        room.server.message = room.server.message || "server connected";
        addIdentityToClients(room.server);
    }
}

function addIdentityToClients(user) {
    for (let i = 0; i < Object.keys(clients).length; i++) {
        if (Object.keys(clients)[i] == user.id) {
            clients[user.id].identity = user.identity;
        }
    };
}


function initSocketConnection() {
    console.log("attempting connection to " + socketServer);
    socket = io(socketServer, { secure: true });
    socket.on("connect", () => {
        console.log("socket.io connected to " + socketServer);
    });

    socket.on("connect_failed", (e) => {
        console.log("connect_failed");
    });

    socket.on("error", (e) => {
        console.log("error: " + e);
    });

    socket.on("introduction", (payload) => {
        iceServerList = payload.iceServers;
        this_client_id = payload.id;
        if (el.client_id) {
            el.client_id.innerHTML = this_client_id + ":(NO IDENTITY)";
        }
        for (let i = 0; i < payload.clients.length; i++) {
            if (payload.clients[i] != this_client_id) {
                addClient(payload.clients[i]);
                callUser(payload.clients[i]);
            }
        }
    });

    socket.on("identity-declared", (payload) => {
        if (payload.id != this_client_id) {
            for (let i = 0; i < Object.keys(clients).length; i++) {
                if (Object.keys(clients)[i] == payload.id) {
                    moveClientToIdentity(clients[payload.id], payload.identity, payload.message);
                    alreadyHasUser = true;
                    break;
                }
            }
        } else {
            el.client_id.innerHTML = this_client_id + ":(" + payload.identity + ")";
        }
    });

    socket.on("newUserConnected", (payload) => {
        console.log("newUserConnected", payload);
        let alreadyHasUser = false;
        for (let i = 0; i < Object.keys(clients).length; i++) {
            if (Object.keys(clients)[i] == payload.id) {
                alreadyHasUser = true;
                break;
            }
        }

        if (payload.id != this_client_id && !alreadyHasUser) {
            addClient(payload.id);
        } else {
            declareIdentity(identity);
        }

        mergeExistingRoom(payload.room);

    });

    socket.on("userDisconnected", (payload) => {
        if (payload.id != this_client_id) {
            const disconnectingUser = findUserInRoom(payload.id);
            if (disconnectingUser != null) {
                removeClientVideoElement(payload.id);
                room[disconnectingUser.identity] = null;
            }
            delete clients[payload.id];
        }
    });

    socket.on("messageSent", (payload) => {
        const messagingUser = findUserInRoom(payload.id);
        if (messagingUser != null) {
            if (messagingUser.identity != identity) {
                if (room[messagingUser.identity] != null) {
                    room[messagingUser.identity].message = payload.message;
                    updateRoomMessages();
                }
            }
        }
    });

    socket.on("call-made", async (data) => {
        console.log("Receiving call", data);
        data.offer.type = convertUnityType(data.offer.type);

        await clients[data.socket].peerConnection.setRemoteDescription(
            new RTCSessionDescription(data.offer)
        );

        const answer = await clients[data.socket].peerConnection.createAnswer();
        window.sdp = data.offer.sdp;
        await clients[data.socket].peerConnection.setLocalDescription(
            new RTCSessionDescription(answer)
        );
        socket.emit("make-answer", {
            answer,
            to: data.socket,
        });
    });

    socket.on("answer-made", async (data) => {
        console.log("Receiving answer ", data);

        if (data.answer.type === "2" || data.answer.type === 2) {
            data.answer.type = "answer";
        }

        await clients[data.socket].peerConnection.setRemoteDescription(
            new RTCSessionDescription(data.answer)
        );

        // preventing double calls. 
        if (!clients[data.socket].isAlreadyCalling) {
            callUser(data.socket);
            clients[data.socket].isAlreadyCalling = true;
        }
    });

    socket.on("iceCandidateFound", (data) => {
        clients[data.socket].peerConnection.addIceCandidate(data.candidate);
    });

    if (el.textarea != null) {
        el.textarea.addEventListener('input', function (event) {
            socket.emit("message", {
                id: this_client_id,
                message: event.target.value
            });
        }, false);
    }
}

function declareIdentity(identity) {
    socket.emit("declare-identity", {
        identity,
        message: "browser version of " + identity + " connected",
        id: this_client_id,
    });
}

function addClient(_id) {
    console.log("adding client with _id " + _id);
    clients[_id] = {};
    clients[_id].id = _id;
    let pc = createPeerConnection(clients[_id]);
    clients[_id].peerConnection = pc;
    clients[_id].isAlreadyCalling = false;
}

function createPeerConnection(_client) {
    const _id = _client.id;
    let peerConnectionConfiguration;
    peerConnectionConfiguration = { iceServers: iceServerList, bundlePolicy: "max-compat" };
    let pc = new RTCPeerConnection(peerConnectionConfiguration);

    pc.ontrack = function ({ streams: [_remoteStream] }) {
        let videoStream = new MediaStream([_remoteStream.getVideoTracks()[0]]);
        if (!_client.videoEl) {
            clients[_id].videoEl = createRemoteAudienceMediaElements(_id);
        }
        if (_client.identity) {
            //has identity
            createRemoteVideo(_client.identity, videoStream);
        } else {
            console.log("no identity for ", _client, clients);
            //video without an identity.
            const remoteVideoElement = document.getElementById(_id + "_video");
            if (remoteVideoElement) {
                remoteVideoElement.srcObject = videoStream;
            } else {
                console.warn("No video element found for ID: " + _id);
            }
        }
    }

    pc.onicecandidate = (e) => {
        if (e.candidate) {
            socket.emit("addIceCandidate", {
                candidate: e.candidate,
                to: _id,
            });
        }
    }

    addTracksToPeerConnection(localMediaStream, pc);
    return pc;
}

async function callUser(id) {
    if (clients.hasOwnProperty(id)) {
        console.log("calling user", clients[id]);
        const offer = await clients[id].peerConnection.createOffer();
        await clients[id].peerConnection.setLocalDescription(
            new RTCSessionDescription(offer)
        );

        socket.emit("call-user", {
            offer,
            to: id,
        });
    }
}

function disableOutgoingStream() {
    localMediaStream.getVideoTracks().forEach((track) => {
        track.enabled = false;
    });
}

function enableOutgoingStream() {
    localMediaStream.getTracks().forEach((track) => {
        track.enabled = true;
    });
}


function createLocalVideoElement() {
    videoElement = document.createElement("video");
    videoElement.id = "local_video";
    videoElement.autoplay = true;
    videoElement.width = videoWidth;
    videoElement.height = videoHeight;
    videoElement.playsInline = true;
    let videoStream = new MediaStream([localMediaStream.getVideoTracks()[0]]);
    videoElement.srcObject = videoStream;
    document.body.appendChild(videoElement);
}




function createRemoteAudienceMediaElements(_id) {
    const videoElement = document.createElement("video");
    videoElement.id = _id + "_video";
    videoElement.width = videoWidth;
    videoElement.height = videoHeight;
    videoElement.autoplay = true;
    videoElement.playsInline = true;
    videoElement.muted = true;
    //document.body.appendChild(videoElement);
    return videoElement;
}


function removeClientVideoElement(_id) {
    let videoEl = document.getElementById(_id + "_video");
    if (videoEl != null) {
        videoEl.remove();
    }
}



function convertUnityType(_type) {
    //dirty hack for unity enums. 
    if (_type === "0" || _type === 0) {
        return "offer";
    }

    if (_type === "2" || _type === 2) {
        return "answer";
    }
    return _type;
}





