// express server
const fs = require('fs');
const express = require("express");
const app = express();
app.use(express.static("public"));


// const options = {
//     key: fs.readFileSync('certs/key.pem'),
//     cert: fs.readFileSync('certs/cert.pem'),
//     ca: fs.readFileSync('certs/chain.pem'),
// };


const room = {
    actor: null,
    director: null,
    server: null,
};



//for localhost
const options = {
    key: fs.readFileSync('certs/server.key'),
    cert: fs.readFileSync('certs/server.crt')
};



var https = require('https');
const port = process.env.PORT || 8080;

const server = https.createServer(options, app).listen(port, () => {
    console.log("listening on " + port);
});


// socket.io
const io = require("socket.io")({
    cors: true
}).listen(server);

let iceServers = [
    { urls: "stun:stun.l.google.com:19302" },
    { urls: "stun:stun1.l.google.com:19302" },
    { urls: "stun:stun2.l.google.com:19302" },
];


let clients = {};

function main() {
    setupSocketServer();
}

function findClientInRoom(client_id) {
    if (room.director != null && room.director.id === client_id) {
        return room.director;
    }
    if (room.actor != null && room.actor.id === client_id) {
        return room.actor;
    }
    if (room.server != null && room.server.id === client_id) {
        return room.server;
    }
    return null;
}


main();

function setupSocketServer() {
    console.log("setting up socket server");
    io.on("connection", (client) => {
        console.log("User " + client.id + " connected");

        clients[client.id] = {
            id: client.id
        };

        client.emit(
            "introduction",
            {
                id: client.id,
                iceServers,
                clients: Object.keys(clients)
            }
        );

        io.sockets.emit(
            "newUserConnected",
            {
                id: client.id,
                clients: Object.keys(clients),
                room,
            }
        );

        client.on("disconnect", () => {
            delete clients[client.id];
            const foundUser = findClientInRoom(client.id);
            if (foundUser != null) {
                room[foundUser.identity] = null;
            }
            io.sockets.emit(
                "userDisconnected",
                {
                    id: client.id,
                    clients: Object.keys(clients)
                }
            );

            console.log(
                "User " + client.id + " disconnected"
            );
        });


        //webrtc communications
        client.on("call-user", (data) => {
            console.log("Server forwarding call from " + client.id + " to " + data.to);
            client.to(data.to).emit("call-made", {
                offer: data.offer,
                socket: client.id
            });
        });

        client.on("make-answer", (data) => {
            client.to(data.to).emit("answer-made", {
                socket: client.id,
                answer: data.answer
            });
        });

        client.on("addIceCandidate", (data) => {
            client.to(data.to).emit("iceCandidateFound", {
                socket: client.id,
                candidate: data.candidate
            });
        });


        client.on("connect_failed", (err) => {
            console.log("connect failed!", err);
        });

        client.on("error", (err) => {
            console.log("there was an error on the connection!", err);
        });

        client.on("declare-identity", (data) => {
            if (data.identity != null) {
                room[data.identity] = {
                    id: data.id,
                    identity: data.identity,
                    message: data.message,
                }
            }
            io.sockets.emit(
                "identity-declared",
                data
            );
            console.log("new identity", room);
        });

        client.on("message", (data) => {
            const messageUser = {
                id: data.id,
                message: data.message,
            }
            const foundUser = findClientInRoom(data.id);
            if (foundUser != null) {
                foundUser.message = data.message;
            }

            io.sockets.emit(
                "messageSent",
                {
                    ...messageUser,
                }
            );
        });

        client.on("cam-coordinates", (data) => {
            if (room.director != null && data.id === room.director.id) {
                io.sockets.emit(
                    "cam-coordinates",
                    data
                );
            }
        });

        client.on("floor-position", (data) => {
            if (room.director != null && data.id === room.director.id) {
                io.sockets.emit(
                    "floor-position",
                    data
                );
            }
        });

        client.on("human-position", (data) => {
            if (room.director != null && data.id === room.director.id) {
                io.sockets.emit(
                    "human-position",
                    data
                );
            }
        });

    });
}